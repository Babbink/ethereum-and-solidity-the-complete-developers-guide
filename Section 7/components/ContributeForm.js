import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import web3 from "../ethereum/web3";
import Campaign from "./../ethereum/campaign";
import { Router } from "./../routes";

export default class ContributeForm extends Component {
  state = {
    value: "",
    isLoading: false,
    errorMessage: "",
  };

  onSubmit = async (event) => {
    event.preventDefault();
    await ethereum.enable();

    const campaign = Campaign(this.props.address);

    this.setState({ isLoading: true, errorMessage: "" });

    try {
      const accounts = await web3.eth.getAccounts();
      await campaign.methods.contribute().send({
        from: accounts[0],
        value: web3.utils.toWei(this.state.value, "ether"),
      });

      Router.replaceRoute(`/campaigns/${this.props.address}`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ isLoading: false});
  };

  render() {
    return (
      <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
        <Form.Field>
          <label>Amount to Contribute</label>
          <Input
            label="ether"
            value={this.state.value}
            onChange={(event) => this.setState({ value: event.target.value })}
            labelPosition="right"
          />
        </Form.Field>
        
        <Message error header="Oops!" content={this.state.errorMessage} />
        <Button primary loading={this.state.isLoading}>
          Contribute!
        </Button>
      </Form>
    );
  }
}
