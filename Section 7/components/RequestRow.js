import React, { Component } from "react";
import { Button, Table } from "semantic-ui-react";
import { Router } from "./../routes";
import web3 from "./../ethereum/web3";
import Campaign from "./../ethereum/campaign";

export default class RequestRow extends Component {
  state = {
    isLoadingApproval: false,
    isLoadingFinalizing: false,
    notReadyToFinalize: true,
    errorMessage: "",
  };

  onApprove = async () => {
    const campaign = Campaign(this.props.address);
    const accounts = await web3.eth.getAccounts();

    this.setState({ isLoadingApproval: true, errorMessage: "" });

    try {
      await campaign.methods
        .approveRequest(this.props.id)
        .send({ from: accounts[0] });

      Router.pushRoute(`/campaigns/${this.props.address}/requests`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ isLoadingApproval: false });
  };

  onFinalize = async () => {
    const campaign = Campaign(this.props.address);
    const accounts = await web3.eth.getAccounts();

    this.setState({ isLoadingFinalizing: true, errorMessage: "" });

    try {
      await campaign.methods
        .finalizeRequest(this.props.id)
        .send({ from: accounts[0] });

      Router.pushRoute(`/campaigns/${this.props.address}/requests`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ isLoadingFinalizing: false });
  };

  render() {
    const { Row, Cell } = Table;
    const { id, request, approversCount, manager } = this.props;
    const readyToFinalize = request.approvalCount > approversCount / 2;

    return (
      <Row
        disabled={request.complete}
        positive={readyToFinalize && !request.complete}
      >
        <Cell>{id + 1}</Cell>
        <Cell>{request.description}</Cell>
        <Cell>{web3.utils.fromWei(request.value, "ether")}</Cell>
        <Cell>{request.recipient}</Cell>
        <Cell>
          {request.approvalCount}/{approversCount}
        </Cell>
        <Cell>
          {request.complete ? (
            "Finalized"
          ) : (
            <Button
              color="green"
              basic
              onClick={this.onApprove}
              loading={this.state.isLoadingApproval}
            >
              Approve
            </Button>
          )}
        </Cell>
        <Cell>
          {request.complete ? (
            "Finalized"
          ) : (
            <Button
              color="teal"
              basic
              onClick={this.onFinalize}
              loading={this.state.isLoadingFinalizing}
            >
              Finalize
            </Button>
          )}
        </Cell>
      </Row>
    );
  }
}
