const assert = require("assert");
const ganache = require("ganache-cli");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());

const compiledFactory = require("./../ethereum/build/CampaignFactory.json");
const compiledCampaign = require("./../ethereum/build/Campaign.json");

let accounts, factory, campaignAddres, campaign;
const maxGas = "1000000";
const minimumContribution = "5";

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({ data: compiledFactory.bytecode })
    .send({ from: accounts[0], gas: maxGas });

  await factory.methods
    .createCampaign(minimumContribution)
    .send({ from: accounts[0], gas: maxGas });

  [campaignAddres] = await factory.methods.getDeployedCampaigns().call();

  campaign = await new web3.eth.Contract(
    JSON.parse(compiledCampaign.interface),
    campaignAddres
  );
});

describe("Campaigns", () => {
  it("deploys a factory and a campaign", () => {
    assert.ok(factory.options.address);
    assert.ok(campaign.options.address);
  });

  it("marks caller as the campaign manager", async () => {
    const manager = await campaign.methods.manager().call();
    assert.strictEqual(manager, accounts[0]);
  });

  it("should set a minimum contribution in a campaign", async () => {
    const campaignMinimumContribution = await campaign.methods
      .minimumContribution()
      .call();

    assert.strictEqual(campaignMinimumContribution, minimumContribution);
  });

  it("requires a minimum contribution", async () => {
    try {
      await campaign.methods
        .contribute()
        .send({ value: 2, from: accounts[1], gas: maxGas });
      assert(false);
    } catch (error) {
      assert(error);
    }
  });

  it("allows a manager to create a payment request", async () => {
    const manager = await campaign.methods.manager().call();
    const description = "Buy components";
    await campaign.methods
      .createRequest(description, "100", accounts[1])
      .send({ from: manager, gas: maxGas });

    const request = await campaign.methods.requests(0).call();
    assert.equal(description, request.description);
  });

  it("allows people to contribute money and marks them as approvers", async () => {
    await campaign.methods
      .contribute()
      .send({ value: 10, from: accounts[1], gas: maxGas });

    assert(await campaign.methods.approvers(accounts[1]).call());
  });

  it.only("processes requests", async () => {
    const manager = await campaign.methods.manager().call();
    const contributer = accounts[1];
    const manufacture = accounts[2];

    await campaign.methods
      .contribute()
      .send({ value: 10, from: contributer, gas: maxGas });

    await campaign.methods
      .createRequest("Buy components", "5", manufacture)
      .send({ from: manager, gas: maxGas });

    await campaign.methods
      .approveRequest(0)
      .send({ from: contributer, gas: maxGas });

    await campaign.methods
      .finalizeRequest(0)
      .send({ from: manager, gas: maxGas });

    const request = await campaign.methods.requests(0).call();
    const manufactureBalance = await web3.eth.getBalance(manufacture);

    assert.ok(request.complete);
    assert.ok(manufactureBalance.includes(5));
  });
});
