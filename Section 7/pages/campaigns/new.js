import React, { Component } from "react";
import { Form, Button, Input, Message } from "semantic-ui-react";
import Layout from "./../../components/Layout";
import web3 from "./../../ethereum/web3";
import factory from "./../../ethereum/factory";
import { Router } from "./../../routes";

export default class CampaignNew extends Component {
  state = {
    minimumContribution: "",
    errorMessage: "",
    isLoading: false,
  };

  onSubmit = async (event) => {
    event.preventDefault();
    await ethereum.enable();

    this.setState({ isLoading: true, errorMessage: "" });

    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .createCampaign(this.state.minimumContribution)
        .send({ from: accounts[0] });

      Router.pushRoute("/");
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ isLoading: false });
  };

  render() {
    return (
      <div>
        <Layout>
          <h3>Create a Campaign!</h3>

          <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
            <Form.Field>
              <label>Minimum Contribution</label>
              <Input
                label="wei"
                labelPosition="right"
                value={this.state.minimumContribution}
                onChange={(event) => {
                  this.setState({
                    minimumContribution: event.target.value,
                  });
                }}
              />
            </Form.Field>

            <Message error header="Oops!" content={this.state.errorMessage} />
            <Button primary loading={this.state.isLoading}>
              Create
            </Button>
          </Form>
        </Layout>
      </div>
    );
  }
}
