import web3 from "./web3";
import CampaignFactory from "./build/CampaignFactory.json";

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0x8444C82922729368EA2719Ae304230A43E758Dc5"
);

export default instance;
